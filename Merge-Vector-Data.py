#   Project:    ArcMap-Merge-Vector-Data
#   Author:     Mike Wunderlich
#   Mail:       GIS@mikewunderlich.de
#   Version:    1.0 - Working

import arcpy
import os


inPaths = arcpy.GetParameterAsText(0)
outGDB = arcpy.GetParameterAsText(1)
path = r'J:\Vektor'
gdb = r'J:\Vektor\res.gdb'



listPath = inPaths.split(";")
listSHP = []
for path in listPath:
    for paths, dirctory, files in os.walk(path):
        for file in files:
            if '.shp' in file:
                if file not in listSHP:
                    listSHP.append(file)
                    
index = 1
for shp in listSHP:
    arcpy.AddMessage("["+str(index)+":"+str(len(listSHP))+"] " + str(shp))
    parts=[]
    for path in listPath:
        item = path+"/"+shp
        if arcpy.Exists(item):
            parts.append(item)
    arcpy.Merge_management(parts, outGDB+"/"+shp.replace(".shp",""))
    index=index+1
